load("//build:build.bzl", "go_custom_test")
load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:proto.bzl", "go_grpc_generate")

go_grpc_generate(
    src = "rpc.proto",
    workspace_relative_target_directory = "internal/module/gitops/rpc",
    deps = [
        "//internal/tool/grpctool/automata:proto",
        "//pkg/agentcfg:proto",
        "@com_github_envoyproxy_protoc_gen_validate//validate:validate_proto",
    ],
)

go_library(
    name = "rpc",
    srcs = [
        "obj_to_sync_watcher.go",
        "rpc.pb.go",
        "rpc.pb.validate.go",
        "rpc_grpc.pb.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/module/gitops/rpc",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/tool/grpctool",
        "//internal/tool/grpctool/automata",
        "//internal/tool/retry",
        "//pkg/agentcfg",
        "@com_github_envoyproxy_protoc_gen_validate//validate:go_custom_library",
        "@org_golang_google_grpc//:grpc",
        "@org_golang_google_grpc//codes",
        "@org_golang_google_grpc//status",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
        "@org_golang_google_protobuf//types/known/anypb",
        "@org_uber_go_zap//:zap",
    ],
)

go_custom_test(
    name = "rpc_test",
    srcs = ["obj_to_sync_watcher_test.go"],
    deps = [
        ":rpc",
        "//internal/tool/retry",
        "//internal/tool/testing/matcher",
        "//internal/tool/testing/mock_rpc",
        "//internal/tool/testing/testhelpers",
        "//pkg/agentcfg",
        "@com_github_golang_mock//gomock",
        "@org_uber_go_zap//zaptest",
    ],
)
